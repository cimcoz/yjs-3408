package com.yjs3408.hr.api.department.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yjs3408.hr.api.department.repository.domain.Department;
import com.yjs3408.hr.api.department.service.DepartmentService;

@RestController
@RequestMapping("/api/v1/department")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@PostMapping
	public ResponseEntity<Department> save(@RequestBody Department entity) {
		return ResponseEntity.ok(departmentService.save(entity));
	}

//	@PutMapping("/{id}")
//	public ResponseEntity<Department> update(@PathVariable Long id, @RequestBody Department entity) {
//		return ResponseEntity.ok(departmentService.save(entity));
//	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		departmentService.delete(departmentService.findByID(id).get());
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	@GetMapping
	public ResponseEntity<List<Department>> findAll() {
		return ResponseEntity.ok(departmentService.findAll());
	}
}
