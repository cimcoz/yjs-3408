package com.yjs3408.hr.api.department.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yjs3408.hr.api.department.repository.DepartmentRepo;
import com.yjs3408.hr.api.department.repository.domain.Department;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentRepo departmentRepo;

	public Department save(Department entity) {
		return departmentRepo.save(entity);
	}

	public Department update(Department entity) {
		return departmentRepo.save(entity);
	}

	public void delete(Department entity) {
		departmentRepo.delete(entity);
	}
	
	public Optional<Department> findByID(long id) {
		return departmentRepo.findById(id);
	}
	
	public List<Department> findAll() {
		return departmentRepo.findAll();
	}
}
