package com.yjs3408.hr.api.department.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yjs3408.hr.api.department.repository.domain.Department;

@Repository
public interface DepartmentRepo extends JpaRepository<Department, Long> {

}
